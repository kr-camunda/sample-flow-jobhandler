package com.coremedia.bizops.bpmn.camunda.worker.message;

import java.io.Serializable;
import java.util.Map;

public class ServiceBusMessage implements Serializable {
    private String result;
    private Long key;
    private String type;
    private int retries;
    private Map<String,Object> payload;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Long getKey() {
        return key;
    }

    public void setKey(Long key) {
        this.key = key;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getRetries() {
        return retries;
    }

    public void setRetries(int retries) {
        this.retries = retries;
    }

    public Map<String, Object> getPayload() {
        return payload;
    }

    public void setPayload(Map<String, Object> payload) {
        this.payload = payload;
    }
}
