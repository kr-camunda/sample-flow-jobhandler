package com.coremedia.bizops.bpmn.camunda.worker;

import com.coremedia.bizops.bpmn.camunda.worker.jobhandler.ServiceBusHandler;
import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;
import org.jboss.logging.Logger;

import javax.inject.Inject;

@QuarkusMain
public class Main {
    public static void main(String... args) {
        Quarkus.run(MyApp.class, args);
    }

    public static class MyApp implements QuarkusApplication {
        private static final Logger LOG = Logger.getLogger(MyApp.class);

        @Inject
        ZeebeService zeebeService;

        @Inject
        ServiceBusService serviceBusService;

        @Override
        public int run(String... args) throws Exception {
            LOG.info("Starting Up");

            zeebeService.registerJobHandler(new ServiceBusHandler(serviceBusService), "find-approver", 30);

            Quarkus.waitForExit();
            return 0;
        }
    }
}
