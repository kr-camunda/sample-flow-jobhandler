package com.coremedia.bizops.bpmn.camunda.worker;

import io.camunda.zeebe.client.api.worker.JobHandler;
import io.camunda.zeebe.client.api.worker.JobWorker;
import io.camunda.zeebe.client.ZeebeClient;
import io.camunda.zeebe.client.impl.oauth.OAuthCredentialsProvider;
import io.camunda.zeebe.client.impl.oauth.OAuthCredentialsProviderBuilder;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import java.time.Duration;
import java.util.Optional;

@ApplicationScoped
public class ZeebeService {
    @ConfigProperty(name = "zeebe.client.api")
    String zeebeAPI;
    @ConfigProperty(name = "zeebe.client.clientId")
    Optional<String> clientId;
    @ConfigProperty(name = "zeebe.client.clientSecret")
    Optional<String> clientSecret;
    @ConfigProperty(name = "zeebe.client.oAuthAPI")
    Optional<String> oAuthAPI;

    private ZeebeClient client;

    private void initClient() {
        if (clientId.isPresent()) {
            OAuthCredentialsProvider credentialsProvider =
                    new OAuthCredentialsProviderBuilder()
                            .authorizationServerUrl(oAuthAPI.get())
                            .audience(zeebeAPI)
                            .clientId(clientId.get())
                            .clientSecret(clientSecret.get())
                            .build();

            client = ZeebeClient.newClientBuilder()
                    .gatewayAddress(zeebeAPI)
                    .credentialsProvider(credentialsProvider)
                    .build();
        } else {
            client = ZeebeClient.newClientBuilder()
                    .gatewayAddress(zeebeAPI)
                    .usePlaintext()
                    .build();
        }
    }

    private ZeebeClient getClient() {
        if (client == null) {
            initClient();
        }
        return client;
    }

    public void completeJob(Long key) {
        getClient().newCompleteCommand(key).send().join();
    }

    public void failJob(Long key, int retries) {
        getClient().newFailCommand(key).retries(retries).send().join();
    }

    public JobWorker registerJobHandler(JobHandler handler, String jobType, int timeout) {
        return getClient().newWorker().jobType(jobType).handler(handler).timeout(Duration.ofSeconds(timeout)).open();
    }
}