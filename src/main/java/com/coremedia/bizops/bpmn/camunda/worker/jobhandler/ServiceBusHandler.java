package com.coremedia.bizops.bpmn.camunda.worker.jobhandler;

import com.coremedia.bizops.bpmn.camunda.worker.ServiceBusService;
import io.camunda.zeebe.client.api.response.ActivatedJob;
import io.camunda.zeebe.client.api.worker.JobClient;
import io.camunda.zeebe.client.api.worker.JobHandler;

public class ServiceBusHandler implements JobHandler {

    ServiceBusService serviceBusService;

    public ServiceBusHandler(ServiceBusService serviceBusService) {
        this.serviceBusService = serviceBusService;
    }

    @Override
    public void handle(JobClient client, ActivatedJob job) throws Exception {
        serviceBusService.sendMessage(job);
    }
}