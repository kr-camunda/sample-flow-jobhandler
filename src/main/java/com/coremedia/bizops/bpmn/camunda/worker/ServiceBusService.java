package com.coremedia.bizops.bpmn.camunda.worker;

import com.coremedia.bizops.bpmn.camunda.worker.message.ServiceBusMessage;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.camunda.zeebe.client.api.response.ActivatedJob;
import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.jms.*;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@ApplicationScoped
public class ServiceBusService implements Runnable {
    private static final Logger LOG = Logger.getLogger(ServiceBusService.class);

    @ConfigProperty(name = "azure.servicebus.queues.job-requests")
    String jobRequestQueue;

    @ConfigProperty(name = "azure.servicebus.queues.job-responses")
    String jobResponseQueue;

    @Inject
    ConnectionFactory connectionFactory;

    @Inject
    ZeebeService zeebeService;

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final ExecutorService scheduler = Executors.newSingleThreadExecutor();

    void onStart(@Observes StartupEvent ev) {
        scheduler.submit(this);
    }

    void onStop(@Observes ShutdownEvent ev) {
        scheduler.shutdown();
    }

    public void sendMessage(ActivatedJob job) {
        try (JMSContext context = connectionFactory.createContext(JMSContext.AUTO_ACKNOWLEDGE)) {
            ServiceBusMessage serviceBusMessage = new ServiceBusMessage();
            serviceBusMessage.setKey(job.getKey());
            serviceBusMessage.setType(job.getType());
            serviceBusMessage.setRetries(job.getRetries());
            serviceBusMessage.setPayload(job.getVariablesAsMap());

            BytesMessage message = context.createBytesMessage();
            message.setLongProperty("jobKey", job.getKey());
            message.setStringProperty("jobType", job.getType());
            message.writeBytes(objectMapper.writeValueAsString(serviceBusMessage).getBytes(StandardCharsets.UTF_8));

            context.createProducer().send(context.createQueue(jobRequestQueue), message);
        } catch (JsonProcessingException e) {
            LOG.error("sendMessage() failed", e);
        } catch (JMSException e) {
            LOG.error("sendMessage() failed", e);
        }
    }

    @Override
    public void run() {
        try (JMSContext context = connectionFactory.createContext(JMSContext.AUTO_ACKNOWLEDGE)) {
            JMSConsumer consumer = context.createConsumer(context.createQueue(jobResponseQueue));
            while (true) {
                try {
                    Message message = consumer.receive();
                    if (message == null) return;

                    String decodedBody = new String(Base64.getDecoder().decode(message.getBody(String.class)));
                    ServiceBusMessage serviceBusMessage = objectMapper.readValue(decodedBody, ServiceBusMessage.class);

                    Long key = message.getLongProperty("jobKey");
                    String result = message.getStringProperty("jobResult");

                    switch(result) {
                        case "SUCCESS":
                            zeebeService.completeJob(key);
                            LOG.info(String.format("Job %d completed", key));
                            break;
                        case "FAIL":
                            zeebeService.failJob(key, serviceBusMessage.getRetries());
                            LOG.info(String.format("Job %d failed", key));
                            break;
                        default:
                            LOG.error(String.format("Unknown Result %s for key %d", result, key));
                    }
                } catch(JMSException e) {
                    LOG.error("message receive() failed, JMS Error", e);
                } catch(Exception e) {
                    LOG.error("message receive() failed, unknown Error", e);
                }
            }
        }
    }
}